Licence
=======

Le contenu de ce dépôt est composé de données personnelles et de notes normalement rédigées par mes soins. Ce contenu n'est donc pas complètement libre. C'est pourquoi il est placé sous licence:

- [texte légal de la licence](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)

## Attribution - Pas d'Utilisation Commerciale - Pas de Modification 4.0 International (CC BY-NC-ND 4.0)

This is a human-readable summary of (and not a substitute for) the [license](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). [Avertissement](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr#).

### Vous êtes autorisé à :

* **Partager** — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
* L'Offrant ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence.

### Selon les conditions suivantes :

* **Attribution** — Vous devez [créditer](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr#) l'Œuvre, intégrer un lien vers la licence et [indiquer](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr#) si des modifications ont été effectuées à l'Œuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'Offrant vous soutient ou soutient la façon dont vous avez utilisé son Œuvre.
 * **Pas d’Utilisation Commerciale** — Vous n'êtes pas autorisé à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
* **Pas de modifications** — Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'Œuvre originale, vous n'êtes pas autorisé à distribuer ou mettre à disposition l'Œuvre modifiée.
 * **Pas de restrictions complémentaires** — Vous n'êtes pas autorisé à appliquer des conditions légales ou des [mesures techniques](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr#) qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.

### Notes:

* Vous n'êtes pas dans l'obligation de respecter la licence pour les éléments ou matériel appartenant au domaine public ou dans le cas où l'utilisation que vous souhaitez faire est couverte par une [exception](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr#).
* Aucune garantie n'est donnée. Il se peut que la licence ne vous donne pas toutes les permissions nécessaires pour votre utilisation. Par exemple, certains droits comme [les droits moraux, le droit des données personnelles et le droit à l'image](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr#) sont susceptibles de limiter votre utilisation.

