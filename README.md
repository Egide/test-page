# Page personnelle d'Égide

*date de création* : 2022/04/19


## Destinataires du projet

Pour un usage personnel, le but de ce projet est seulement d'avoir un endroit
pour mettre mes notes personnelles.


## Ce que le projet fait

Génère un site Web avec MkDocs qui est hébergé sur https://egide.framagit.io


## Prérequis

### Éthiques

Travailler sur GNU/Linux, ou autre système libre. Dans le cas contraire vous
sortirez d'ici couvert de goudron et de plume...


### Matériels

- tout type de matériel


### Logiciel

- GIT
- gitflow
- MkDocs
- MkDocs-material


## Documentation

1. [LICENSE](LICENSE) termes de la licence
2. [CHANGELOG](CHANGELOG.md) Changements importants d'une version à l'autre
3. [CONTRIBUTING](CONTRIBUTING.md) indications utiles pour les personnes qui voudraient contribuer au projet


